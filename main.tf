# Taken from example from:
# https://github.com/terraform-aws-modules/terraform-aws-lambda/tree/v2.17.0/examples/container-image#module_docker_image


# Create lambda from container
module "lambda_function_from_container_image" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "2.17.0"

  function_name = var.lambda_function_name
  description   = var.lambda_description

  create_package = false

  image_uri    = var.image_uri
  package_type = "Image"

  lambda_role               = var.iam_role_name
  role_permissions_boundary = var.role_permissions_boundary

  create_role = false

  handler     = var.cronjob_handler
  timeout     = var.cronjob_timeout
  memory_size = var.cronjob_memory_size

  image_config_command           = var.image_config_command
  image_config_entry_point       = var.image_config_entry_point
  image_config_working_directory = var.image_config_working_directory

  vpc_security_group_ids = var.ecs_security_group_ids
  # Every subnet should be able to reach an EFS mount target in the same Availability Zone. Cross-AZ mounts are not permitted.
  vpc_subnet_ids = var.private_subnet_ids

  environment_variables = var.lambda_env_variables

  tags = var.tags
}

resource "aws_cloudwatch_event_rule" "cronjob_event_rule" {
  name                = var.cronjob_name
  description         = var.cronjob_description
  schedule_expression = var.cronjob_expression
  tags                = var.tags
}

resource "aws_cloudwatch_event_target" "cronjob_event_target" {
  target_id = var.target_id
  rule      = aws_cloudwatch_event_rule.cronjob_event_rule.name
  arn       = module.lambda_function_from_container_image.lambda_function_arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_check_foo" {
  statement_id  = var.aws_lambda_permission_statement_id
  action        = "lambda:InvokeFunction"
  function_name = module.lambda_function_from_container_image.lambda_function_arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cronjob_event_rule.arn
}
