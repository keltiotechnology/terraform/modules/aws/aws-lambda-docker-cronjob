# data "terraform_remote_state" "infra" {
#   backend = "remote"

#   config = {
#     organization = "MobileHub"
#     workspaces = {
#       name = "mobilehub-infra-${terraform.workspace}"
#     }
#   }
# }

locals {
  cronjob_allowed_secrets_arn = [
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:MARIADB_DB-4r5okp",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:EMAILS_ALERT-OGTUqq",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:APP_SECRET-qLQZCF",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:CHR_ACCOUNT-gSuekt",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:CHR_PWD-Q9ibge",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:AFTERSHIP_KEY-RYDTR4",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:SECRET_KEY-XCuvjH",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:MAILER_DSN-DzSNay",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:MARIADB_USER-wV2p6o",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:MARIADB_PASSWORD-YPs5uZ",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:MARIADB_RABBITM_USER-4bE1ZU",
    "arn:aws:secretsmanager:us-east-1:713436224091:secret:MARIADB_RABBITM_PASSWORD-emcml3"
  ]
}

provider "aws" {
  region = "us-east-1"

  # Make it faster by skipping something
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true

  profile = "default"
}

/* ------------------------------------------------------------------------------------------------------------ */
/* 2. Create IAM Role + Role Policy for IAM Role                                                                */
/* ------------------------------------------------------------------------------------------------------------ */

resource "aws_iam_role" "iam_for_lambda" {
  name = "cronjob" #var.lambda_cronjob_iamrole

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "iam_lambda_secret_manager_role_policy" {
  name = "Cronjob_policy_for_lambda_iam_role"
  role = aws_iam_role.iam_for_lambda.id
  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "ec2:DescribeNetworkInterfaces",
          "ec2:CreateNetworkInterface",
          "ec2:DeleteNetworkInterface",
          "ec2:DescribeInstances",
          "ec2:AttachNetworkInterface"
        ],
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "secretsmanager:DescribeSecret",
          "secretsmanager:GetSecretValue"
        ],
        "Resource" : local.cronjob_allowed_secrets_arn
      }
    ]
  })
}


module "lambda_docker_cronjob_stock_inventory" {
  source = "../../"

  iam_role_name             = aws_iam_role.iam_for_lambda.arn
  role_permissions_boundary = null
  ecs_security_group_ids    = []
  private_subnet_ids        = []

  aws_lambda_permission_statement_id = "Lambda_Cronjob_ABC"

  cronjob_name        = "MyCronJob"
  cronjob_description = "This cronjob does ABC"
  cronjob_expression  = "cron(0 20 * * ? *)"

  cronjob_handler = ""
  cronjob_timeout = "600"

  image_config_entry_point = ["sh", "entryscript.sh"] # Entrypoint override
  image_config_command     = ["php", "/var/www/bin/console app:stock:inventory"]

  image_uri = "713436224091.dkr.ecr.us-east-1.amazonaws.com/cronjob_symfony:latest"

  lambda_function_name = "my_lambda_func_docker_stock_inventory"

  lambda_env_variables = {
    "MARIADB_DB_ARN"               = local.cronjob_allowed_secrets_arn[0],
    "MARIADB_DB_ARN"               = local.cronjob_allowed_secrets_arn[0],
    "EMAILS_ALERT_ARN"             = local.cronjob_allowed_secrets_arn[1],
    "APP_SECRET_ARN"               = local.cronjob_allowed_secrets_arn[2],
    "CHR_ACCOUNT_ARN"              = local.cronjob_allowed_secrets_arn[3],
    "CHR_PWD_ARN"                  = local.cronjob_allowed_secrets_arn[4],
    "AFTERSHIP_KEY_ARN"            = local.cronjob_allowed_secrets_arn[5],
    "SECRET_KEY_ARN"               = local.cronjob_allowed_secrets_arn[6],
    "MAILER_DNS_ARN"               = local.cronjob_allowed_secrets_arn[7],
    "MARIADB_USER_ARN"             = local.cronjob_allowed_secrets_arn[8],
    "MARIADB_PASSWORD_ARN"         = local.cronjob_allowed_secrets_arn[9],
    "MARIADB_RABBITM_USER_ARN"     = local.cronjob_allowed_secrets_arn[10],
    "MARIADB_RABBITM_PASSWORD_ARN" = local.cronjob_allowed_secrets_arn[11]
  }
  lambda_description = "Execute cronjob for ABC"

  tags = {
    "namespace" = "dev"
  }

  target_id = "Cronjob_event_target"

}
